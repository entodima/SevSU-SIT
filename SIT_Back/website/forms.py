from django import  forms


class LoginForm(forms.Form):
    username = forms.Field(label='', widget=forms.TextInput(attrs={
		'placeholder': 'Login',
	}))
    password = forms.Field(label='', widget=forms.PasswordInput(attrs={
		'placeholder': 'Password',
	}))