from django.urls import path
from website import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.auth, name='auth'),
    path('all/', views.all, name='all'),
    path('macros/', views.macros, name='macros'),
    path('macros/<int:id>/switch', views.macros_switch, name='macros_switch'),
    path('room/<int:id>/', views.room, name='room'),
    path('sensor/<int:id>/switch/', views.sensor_switch, name='sensor_switch'),
    path('sensor/<int:id>/switch-star/', views.sensor_switch_star, name='sensor_switch-star')
]