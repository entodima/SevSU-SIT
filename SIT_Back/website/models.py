from django.conf import settings
from django.db import models

# Create your models here.


class Room(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        default='1'
    )
    title = models.CharField(max_length=20, default='Комната')


class Sensor(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    title = models.CharField(max_length=20)
    value = models.IntegerField(null=True, default=None)
    active = models.BooleanField(default=False)
    stared = models.BooleanField(default=False)


class MacrosSensor(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    type = models.IntegerField(default=2, null=False)

