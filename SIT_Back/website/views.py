from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from website.forms import *
from website.models import *
from django.contrib.auth import authenticate, login, logout
# Create your views here.


def auth(request):
    if request.user.is_authenticated:
        return redirect('index')
    login_form = LoginForm()

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('index')
    return render(request, 'website/auth.html', {'login_form': login_form, })


@login_required(login_url='login/')
def index(request):
    rooms = Room.objects.filter(user=request.user)
    sensors = Sensor.objects.filter(room__user=request.user, stared=True)
    return render(request, 'website/index.html', {
        'rooms': rooms,
        'sensors': sensors,
    })


@login_required(login_url='login/')
def all(request):
    rooms = Room.objects.filter(user=request.user)
    rooms_sensors = []
    for room in rooms:
        rooms_sensors.append([room, Sensor.objects.filter(room=room)])

    return render(request, 'website/all.html', {
        'rooms_sensors': rooms_sensors,
    })


@login_required(login_url='login/')
def room(request, id, sensor_id=None):
    room = get_object_or_404(Room, id=id)
    sensors = Sensor.objects.filter(room_id=id)

    return render(request, 'website/room.html', {
        'room': room,
        'sensors': sensors,
    })


@login_required(login_url='login/')
def sensor_switch(request, id):
    sensor = get_object_or_404(Sensor, id=id)
    sensor.active = not sensor.active
    sensor.save()
    return redirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='login/')
def sensor_switch_star(request, id):
    sensor = get_object_or_404(Sensor, id=id)
    sensor.stared = not sensor.stared
    sensor.save()
    return redirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='login/')
def macros(request):
    macroses_on = MacrosSensor.objects.filter(sensor__room__user=request.user, type=1)
    macroses_off = MacrosSensor.objects.filter(sensor__room__user=request.user, type=0)
    macroses_switch = MacrosSensor.objects.filter(sensor__room__user=request.user, type=2)

    return render(request, 'website/macros.html', {
        'macroses_on': macroses_on,
        'macroses_off': macroses_off,
        'macroses_switch': macroses_switch,
    })


@login_required(login_url='login/')
def macros_switch(request, id=None):
    if (id is not None) and (id in range(0, 3)):
        macroses=MacrosSensor.objects.filter(sensor__room__user=request.user, type=id)
        for macros in macroses:
            sensor = macros.sensor
            if id == 0:
                sensor.active = False
            if id == 1:
                sensor.active = True
            if id == 2:
                sensor.active = not sensor.active
            sensor.save()
            print(id)

    return redirect(request.META.get('HTTP_REFERER'))
