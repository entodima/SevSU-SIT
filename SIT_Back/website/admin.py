from django.contrib import admin
from website.views import *

# Register your models here.

admin.site.register(Room)
admin.site.register(Sensor)
admin.site.register(MacrosSensor)

